﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UAutonomaA.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace UAutonomaA.DAL
{
    public class UAutonomaContext : DbContext
    {
        public UAutonomaContext() : base("UAutonomaContext")
        {

        }

        public DbSet<Estudiante> Estudiantes { set; get; }
        public DbSet<Inscripcion> Inscripcions { set; get; }
        public DbSet<Programa> Programas { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
         /*   Database.SetInitializer<UAutonomaContext>(null);
            base.OnModelCreating(modelBuilder);*/
        }
    }
}