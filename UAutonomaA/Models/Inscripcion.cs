﻿using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace UAutonomaA.Models
{
    public enum Tipo_Aspirante
    {
        Nuevo, Reingreso, TransferenciaInterna, TransferenciaExterna
    }  

    public class Inscripcion
    {
        
        public int ID { set; get; }
        public int EstudianteID { set; get; }
        public int ProgramaID { set; get; }

        [Required(ErrorMessage = "Atributo Requerido")]
        public string Periodo { set; get; }

        [DisplayFormat(NullDisplayText = "Sin tipo")]
        [Required(ErrorMessage = "Atributo Requerido")]
        public Tipo_Aspirante Tipo_aspirante { set; get; }

        [Required(ErrorMessage = "Atributo Requerido")]
        public string Estado { set; get; }

        
        public virtual Estudiante Estudiante { set; get; }        
        public virtual Programa Programa { set; get; }

        
    }

    /*public class InscripcionDbContext : DbContext
    {
        public DbSet<Inscripcion> Inscripcions { set; get; }
    }*/

   


}