﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UAutonomaA.Models
{
    public enum Modalidad
    {
        Virtual, Presencial, SemiPresencial
    }

    public enum Sede
    {
        Medellin, Pereira
    }

    public class Programa
    {
        public int ID { set; get; }

        [Required]
        [Display(Name = "Nombre del Programa")]
        public string Nombre { set; get; }

        [Required]
        [Display(Name = "Sede del Programa")]
        public string Sede { set; get; }

        [Required]
        public Modalidad Modalidad { set; get; }
    }
    /*
    public class ProgramaDbContext : DbContext
    {
        public DbSet<Programa> Programas { set; get; }
    }*/
}