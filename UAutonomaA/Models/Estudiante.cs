﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UAutonomaA.Models
{
    public enum Sexo
    {
        Masculino, Femenino, Otro
    }

    public enum Estado_Civil
    {
        Soltero, Casado, UnionLibre, Viudo
    }

    public enum Grupo_sanguineo
    {
        APositivo, ANegativo, BPositivo, BNegativo, Otro
    }

    public enum Tipo_documento
    {
        Cedula, Pasaporte, TarjetaIdentidad, LibretaMilitar
    }

    public class Estudiante
    {
        public int ID { set; get; }

        [Required, Display(Name = "Tipo de Documento")]
        public Tipo_documento Tipo_doc { set; get; }

        [Required,Display(Name = "Numero de Documento")]
        public int Numero_doc { set; get; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Expedicion")]
        public DateTime Fecha_exp { set; get; }

        [Display(Name = "Pais de Expedicion")]
        public string Pais_exp { set; get; }

        [Display(Name = "Departamento de Expedicion")]
        public string Dpto_exp { set; get; }

        [Display(Name = "Ciudad de Expedicion")]
        public string Ciudad_exp { set; get; }

        public Sexo Sexo { set; get; }



        [Required, Display(Name = "Nombre del Estudiante")]
        public string Primer_nombre { set; get; }

        public string Segundo_nombre { set; get; }

        [Required]
        public string Primer_apellido { set; get; }
        public string Segundo_apellido { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Nacimiento")]
        public DateTime Fecha_nacimiento { set; get; }

        public string Pais { set; get; }
        public string Departamento { set; get; }
        public string Ciudad { set; get; }
        
        public int Telefono { set; get; }

        [Required]
        public int Celular { set; get; }

        [Required]
        public string Correo { set; get; }

        public Grupo_sanguineo Grupo_Sanguineo { set; get; }
        public Estado_Civil Estado_Civil { set; get; }
        
    }

  /*  public class EstudianteDBContext : DbContext
    {
        public DbSet<Estudiante> Estudiantes { set; get; }
    }*/
}