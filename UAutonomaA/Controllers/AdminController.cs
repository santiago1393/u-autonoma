﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UAutonomaA.DAL;
using UAutonomaA.Models;

namespace UAutonomaA.Controllers
{
    public class AdminController : Controller
    {
        private UAutonomaContext db = new UAutonomaContext();

        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.Message = "Pagina de Administracion..";
            return View();
        }

        public ActionResult Aspirantes()
        {
            return View();
        }

        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
        public ActionResult Login()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            try
            {
                if(user == "" || pass == "")
                {
                    ModelState.AddModelError("", "Usuario y Password son requeridos");
                    return View();
                }
                if(user == "admin" && pass == "admin")
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Pista: admin admin");
                    return View();
                }
                
            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Listado()
        {
            List<Inscripcion> inscripcions = db.Inscripcions.ToList();
            foreach (var item in inscripcions)
            {
                item.Programa = db.Programas.Find(item.ProgramaID);
                item.Estudiante = db.Estudiantes.Find(item.EstudianteID);
            }
            return View(inscripcions);
        }
    }
}
