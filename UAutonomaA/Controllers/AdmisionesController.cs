﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UAutonomaA.Models;
using UAutonomaA.DAL;

namespace UAutonomaA.Controllers
{
    public class AdmisionesController : Controller
    {
        private UAutonomaContext db = new UAutonomaContext();
        //private ProgramaDbContext dbProgramas = new ProgramaDbContext();
        private Inscripcion inscripcion = new Inscripcion();
        //private EstudianteDBContext dbEstudiante = new EstudianteDBContext();

        // GET: Admisiones
        public ActionResult Index()
        {
            try
            {
                ViewBag.Message = TempData["Messages"].ToString();
            }
            catch (Exception)
            {
                ViewBag.Message = string.Empty;
               
            }
            return View();
        }

        // GET: Programas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Programas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,nombre_programa,sede")] Inscripcion inscripcion)
        {
            if (ModelState.IsValid)
            {
                db.Inscripcions.Add(inscripcion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inscripcion);
        }

        
        public ActionResult Registro()
        {
            try
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            catch (Exception)
            {
                ViewBag.Error = string.Empty;
            }
            /* @Html.EnumDropDownListFor(model => model.Tipo_aspirante, new { htmlAttributes = new { @class = "form-control" } }) */
            if ( this.db.Programas.Count() < 1)
            {
                TempData["Messages"] = "Error, aun no hay programas registrados por el adminsitrador";
                return RedirectToAction("Index");
            }
            else
            {
               
                ViewBag.Programas = new SelectList(this.db.Programas.ToList(), "ID", "Nombre");
                ViewBag.Tipo_aspirantes = new SelectList(Enum.GetValues(typeof(Tipo_Aspirante)));
                ViewBag.Modalidades = new SelectList(Enum.GetValues(typeof(Modalidad)));
                ViewBag.Sedes = new SelectList(Enum.GetValues(typeof(Sede)));
                ViewBag.Periodos = new SelectList(new List<string> { "20191", "20192", "20201", "20202" }); 
            }
            this.inscripcion.Programa = new Programa();
            this.inscripcion.Estudiante = new Estudiante();
            return View();
        }

        public ActionResult Personal()
        {
            try
            {
                if ((bool)TempData["registro"])
                {
                    return View();
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                TempData["Messages"] = "Error, proceso de inscripcion sin iniciar";
                return RedirectToAction("Index");               
            }          
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Personal([Bind(Include = "ID,Tipo_doc,Numero_doc,Fecha_exp,Pais_exp,Dpto_exp,Ciudad_exp,Sexo,Primer_nombre,Segundo_nombre,Primer_apellido,Segundo_apellido,Fecha_nacimiento,Pais,Departamento,Ciudad,Telefono,Celular,Correo,Grupo_Sanguineo,Estado_Civil")] Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                this.db.Estudiantes.Add(estudiante);
                this.db.SaveChanges();
                this.inscripcion = (Inscripcion)TempData["Inscripcion"];
                this.inscripcion.EstudianteID = estudiante.ID;
                this.inscripcion.Estado = "Inscrito";
                this.db.Inscripcions.Add(this.inscripcion);
                this.db.SaveChanges();
                TempData["Messages"] = "Registro realizado con exito #registro: " + this.inscripcion.ID ;
                return RedirectToAction("Index");
            }

            return View(estudiante);
        }

        [HttpPost]      
        public ActionResult Registro(string  tipo_aspirante, string modalidad, string sede, string periodo, string programa)
        {
            if( string.IsNullOrEmpty(tipo_aspirante) || string.IsNullOrEmpty(modalidad) ||  string.IsNullOrEmpty(sede) || string.IsNullOrEmpty(periodo) || string.IsNullOrEmpty(programa))
            {
                //ModelState.AddModelError("", "Debes completar los campos requeridos");
                TempData["Error"] = "Te hacen falta Atributos Requeridos";
                return RedirectToAction("Registro");
            }
            
            this.inscripcion.Periodo = periodo;            
            this.inscripcion.ProgramaID = int.Parse(programa);
            Tipo_Aspirante tipo;
            if(Enum.TryParse(tipo_aspirante, out tipo)){
                this.inscripcion.Tipo_aspirante = tipo;
            }
            TempData["Inscripcion"] = inscripcion;
            TempData["registro"] = true;
            return RedirectToAction("Personal");
        }

        public Boolean RegistrarInscripcion(Estudiante estudiante, int programaId, string periodo, string estado)
        {      
            db.Estudiantes.Add(estudiante);         
            int idEst = db.SaveChanges();
            Inscripcion inscripcion = new Inscripcion();           
            inscripcion.Periodo = periodo;            
            db.Inscripcions.Add(inscripcion);
            db.SaveChanges();
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}